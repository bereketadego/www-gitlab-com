---
layout: handbook-page-toc
title: "TAM Segment: Digital Touch"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

⚠️ This information is under active development and is not yet final. This page will be updated on an ongoing basis during this phase.
{: .alert .alert-warning}

## Overview

Customer engagement through digital and programmatic communication and enablement.

## Motions

### Align

Success for this cohort is primarily reported upon through product usage data insights, and through NPS/CSAT survey results.

### Enable

We will seek to remove adoption roadblocks and poor setup scenarios (frequent issue) and enable use-case/stage adoption maturity through admin enablement via email.  In FY22 we will augment this email program with other vehicles such as downloadable issue boards, guided video paths, guided blog series, and Slack Bots.

#### Metrics for Enable

1. Metric: Open Rate
1. Metric: Click-Through Rate
1. Metric: [Time to 1st Value](/handbook/customer-success/tam/onboarding/#time-to-first-value)
1. Metric: End of Onboarding NPS & CSAT results

### Expand

We have legal permission to email users for enablement only, so will seek to drive expansion through suggested next-steps via the FY22 vehicles such as issue boards with task lists, while continuing to partner with other teams such as the Marketing and Growth teams on use case adoption and maturity with key next steps into new stages via key points such as introducing SAST/DAST scanning (leading to Secure, Ultimate).

#### Metrics for Expand

1. Metric: Faster time to IACV growth against FY21 cohort
